const { test, expect } = require('@playwright/test');

test.beforeEach(async ({ page }) => {
  await page.goto('https://rentry.co/');
});

const PASTE_CONTENT = "Example paste for testing"

test.describe('Rentry UI testing', () => {
  test('Create paste', async ({ page }) => {
    await page.locator('#text > div > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > pre > span').press('a'); 
    await page.locator('#text > div > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > pre > span').fill(PASTE_CONTENT); 
    // Create paste
    await page.locator('#submitButton').click();
    // Check that content is as given
    await expect(page.locator('body > div > div > div.col-12 > div:nth-child(1) > div > div > article > div > p')).toHaveText(PASTE_CONTENT);
  });
});
