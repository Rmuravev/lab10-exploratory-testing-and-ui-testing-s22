# Lab10 - Explaratory testing and UI

## Introduction

Hey, hey, hey, what's up guys, it is almost last lab for the course here we go. Imagine your product is done, you've tested and analysed almost everything you've could, and here we have the last step in our process: UI and exploratory testing.  **Let's roll!**

## Explaratory testing

Basically, if you want to do an exploratory testing, you need to pick a use case, for example to book a ticket for "Star Wars XX: THE REVENGE OF THE VENGEANCE SKYWALKER SAGA FINAL" in a cinema, and you need to describe path you need to walk through to get what you want, marking all errors or inconsistensies you've found in the way and document each step passed.

## UI testing

Ok, after you've done previous step and you understand what actions you need to take to achieve your target you may test your UI, to check that each critical component is in place for you to accomplish needed User Story. Usually Selenium is being used - it is a multilanguage library which allows to do Unit Testing, walk through website components and imitate human behaviour. In some kind it is an automated Exploratory testing.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab10 - Explaratory testing and UI
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab10-exploratory-testing-and-ui-testing)
2. There are a lot of different ways to do the exploratory testing, such as:
 + Landmark tour(visit the most important features, just walking through use cases)
 + Antisocial tour(break everything that moves, handle yourself as a real app user, enter anything anywhere)
 + Supermodel tour(check UI of the app on each tool, any insufficiencies with requirements or with common sense)
 + etc, there are a lot of them...
3. We are going to work only with Landmark tours, here how you do it, develop the Use Case(or pick one, if you already have them), next you should create table like this for each path you are taking to accomplish described use case:
![table image](https://i.ibb.co/DgStckd/landmark-testing.png)
4. Ok, after you've created exploratory tests, it is ledgit thought to automatize some of your testing, especially for the most used and critical pathes, like login, core functionality, etc. To do it there exists a whole family of different frameworks, we are going to use Selenium for that purpose. Selenium is a whole set of products which allows your programm to run a website/application, parse them and accomplish different activities. 
   + First, we will need to download and install/unpack webdriver, which we are going to use to imitate our browser. We are going to use Firefox driver - gecko. You can dowload latest verion [here](https://github.com/mozilla/geckodriver/releases)
   + Next, if you are losing Linux, we need to add our webdriver to the system paths, you may do it like this:
```sh
export PATH=$PATH:_path_to_webdriver_
```
   + Ok, we are done with preps, now we can start the fun part, let's create maven project like the previous time and let's add this to our `pom.xml`:
```xml
    <properties>
        <java.version>13</java.version>
        <maven.compiler.source>13</maven.compiler.source>
        <maven.compiler.target>13</maven.compiler.target>
        <testng.version>6.8.7</testng.version>
        <selenium.version>2.39.0</selenium.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>7.1.0</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-java</artifactId>
            <version>3.141.5</version>
        </dependency>
    </dependencies>
```
   + That's it, we are ready for UI testing, let's create `TestGoogle.java` file in test directory, it should look like this:
```java
package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestGoogle {

    @Test
    public void testGoogle() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Google", driver.getTitle());
        WebElement findLine = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[1]/div/div[2]/input"));
        findLine.sendKeys("SQR course is the best");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[3]/center/input[1]"));
        searchButton.click();
        findLine = driver.findElement(By.xpath("/html/body/div[4]/form/div[2]/div[1]/div[2]/div/div[2]/input"));
        Assert.assertEquals("SQR course is the best", findLine.getAttribute("value"));
        driver.quit();
    }
}
```
That's it, we may run our test with `mvn test` and it should autotest Google webpage, we're done.

## Homework

Tested website: https://rentry.co/

Website that is similar to pastebin for publishing paste(s)

### Test cases:

#### **Test Tour 1**

Object to be tested: Create paste

Tester: Ruslan Muravev

Test duration: 0.9 sec (automated)

Further testing opportunities: Create paste with markdown tags

**Implemented via Playwright and automated and used in gitlab ci**

To start test:

```shell 
npm install
npm run test
```


| Nr   | What done | Status | Comments |
| ---- | --------- | ------ | ------ |
| 1 | Open tested website | OK |        |
| 2 | Type "Example paste for testing" in text window | OK |        |
| 3    | Type "Example edit code" in text field with "Custom edit code" placeholder | OK     |          |
| 4    | Click on button with text "Go"                               | OK     |          |
| 5    | Wait redirection on the paste page                           | OK     | |
| 6 | Check if content of text window is "Example paste for testing" | OK | |

**Test Tour 2**

Object to be tested: Export as PDF

Tester: Ruslan Muravev

Test duration: 6 sec (manual)

Further testing opportunities:  -

| Nr   | What done                                | Status | Comments |
| ---- | ---------------------------------------- | ------ | -------- |
| 1    | Open page with created paste             | OK     |          |
| 2    | Click on button with text "Export"       | OK     |          |
| 3    | Click on popup window with "PDF" text    | OK     |          |
| 4    | Wait redirection to PDF file             | OK     |          |
| 5    | Check if content is the same as in paste | OK     |          |

**Test Tour 3**

Object to be tested: Delete paste

Tester: Ruslan Muravev

Test duration: 15 sec (manual)

Further testing opportunities:  Fail to delete paste that created by other user

| Nr   | What done                                                    | Status | Comments |
| ---- | ------------------------------------------------------------ | ------ | -------- |
| 1    | Open page with created paste                                 | OK     |          |
| 2    | Click on button with text "Edit"                             | OK     |          |
| 3    | Wait redirection on the edit page                            | OK     |          |
| 4    | Fill in text field that have "Enter edit code" placeholder by typing "Example edit code" | OK     |          |
| 5    | Click on button with text "Delete"                           | OK     |          |
| 6    | Wait deleting modal window to popup                          | OK     |          |
| 7    | Confirm deleting by clicking on "Delete" button in modal window | OK     |          |
| 8    | Try to open the page with the paste                          | OK     |          |
| 9    | Get 404 error page                                           | OK     |          |

